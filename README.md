![Rocket.Chat logo](https://rocket.chat/images/logo/logo-dark.svg?v2)

# Rocket.Chat CI

### Background

To create and maintain a continuous integration (CI) workflow for Rocket.Chat - adapting to the ever-changing requirements of our users community and core development team.

It become clear from community feedback that Rocket.Chat deployment needs a lot of improvement.  Community members are repeatedly reporting build or memory exhaustion during their attempts to deploy Rocket.Chat. We realize it is silly to require build-from-source-code for every Rocket.Chat deployment.

### Mission

With Rocket.Chat.CI, we aim to:

>  Build Rocket.Chat once, so the world doesn't have to.  

The build workflow will test and produce artifacts that are ready to deploy, without rebuilding, for supported target platforms and environments.

Everything in Rocket.Chat.CI  will be 100% FOSS, and developed under the liberal MIT license by our core team and the extended global team of expert contributors. 



